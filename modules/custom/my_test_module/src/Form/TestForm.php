<?php

namespace Drupal\my_test_module\Form;


use \IndicoIo\IndicoIo as IndicoIo;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class TestFrom.
 */
class TestForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'test_form_id';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['enter_your_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Enter your message!'),
      '#description' => $this->t('User message'),
      '#default_value' => 'Hello!',
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    IndicoIo::$config['api_key'] = 'c7db07198720e897afc9e165373a643d';
    $result = max(IndicoIo::emotion($form_state->getValue('enter_your_message')));
    foreach (IndicoIo::emotion($form_state->getValue('enter_your_message')) as $key => $value) {
      if ($value === $result) {
        $output = $key;
      }
    }
    drupal_set_message(print_r($output, TRUE));
  }
}
